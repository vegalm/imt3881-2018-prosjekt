#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt

def inpaint(org, copy, mask):
 
	copy = laplacebilde(copy)
	org = org*(1-mask) + (copy*mask)

	return org



def laplacebilde(bilde):
	alpha = .05

	laplace = np.zeros(np.shape(bilde))
	laplace = (bilde[0:-2, 1:-1] +
		bilde[2:, 1:-1] +
		bilde[1:-1, 0:-2] +	
		bilde[1:-1, 2:] -
		4 * bilde[1:-1, 1:-1])
	bilde[1:-1, 1:-1] += alpha * laplace
	#bilde[:, 0] = bilde[:, 1]      # Neumann boundary
	#bilde[:, -1] = bilde[:, -2]    #
	#bilde[0, :] = bilde[1, :]      #
	#bilde[-1, :] = bilde[-2 , :]   #
	
	return bilde


def demosaicing(im):
	# Read image and add noise

	imc2 = np.copy(im)

	mosaic = np.zeros(np.shape(im[:, :, 0]))		#Alloker plass
	mosaic[ ::2 , ::2] = im[ ::2 , ::2 ,0]			#R - kanal
	mosaic[1::2 , ::2] = im[1::2 , ::2 ,1]			#G - kanal
	mosaic[ ::2 , 1::2] = im[ ::2 , 1::2 ,1]		#G - kanal
	mosaic[1::2 , 1::2] = im[1::2 , 1::2 ,2]		#B - kanal

	imc = np.zeros(np.shape(im))
	imc[ ::2 , ::2 ,0] = mosaic[ ::2 , ::2]			#R - kanal
	imc[1::2 , ::2 ,1] = mosaic[1::2 , ::2]			#G - kanal
	imc[ ::2 , 1::2 ,1] = mosaic[ ::2 , 1::2]		#G - kanal
	imc[1::2 , 1::2 ,2] = mosaic[1::2 , 1::2]		#B - kanal

	mask = np.ones(np.shape(imc))
	mask[ ::2 , ::2, 0] = 0
	mask[1::2 , ::2, 1] = 0
	mask[ ::2 , 1::2, 1] = 0
	mask[1::2 , 1::2, 2] = 0

	plt.ion()
	data = plt.imshow(imc, plt.cm.gray)
	plt.draw()

	for i in range(100):

		imc[:, :, 0] = inpaint(imc2[:, :, 0], imc[:, :, 0], mask[:, :, 0])
		imc[:, :, 1] = inpaint(imc2[:, :, 1], imc[:, :, 1], mask[:, :, 1])
		imc[:, :, 2] = inpaint(imc2[:, :, 2], imc[:, :, 2], mask[:, :, 2])

		data.set_array(imc)
		plt.draw()
		plt.pause(1e-4)
	plt.close()