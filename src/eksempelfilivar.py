#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

# Read image and add noise

im = plt.imread('lena.png')

# Initialize plotting

plt.ion()
data = plt.imshow(im, plt.cm.gray)
plt.draw()

# Solve diffusion equation

alpha = .25                     # dt / dx**2
while True:
    laplace = (im[0:-2, 1:-1] +
               im[2:, 1:-1] +
               im[1:-1, 0:-2] +
               im[1:-1, 2:] -
               4 * im[1:-1, 1:-1])
    im[1:-1, 1:-1] += alpha * laplace
    im[:, 0] = im[:, 1]      # Neumann boundary
    im[:, -1] = im[:, -2]    #
    im[0, :] = im[1, :]      #
    im[-1, :] = im[-2 , :]   #
    data.set_array(im)
    plt.draw()
    plt.pause(1e-4)