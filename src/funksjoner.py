#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

def printmeny():

	print("""Hva vil du gjore?
		1 - glatting
		2 - inpainting
		3 - kontrastforsterkning
		4 - demosaicing
		5 - somloskloning
		6 - printmeny
		9 - exit""")

def lesint(min, max):
	numb = int(input("tall ({}-{})".format(min, max)))

	while(numb < min or numb > max):
		printmeny()
		numb = input("tall ({}-{})".format(min, max))
		numb = int(numb)

	return numb

def laplacebilde(bilde):
	
	laplace = (bilde[0:-2, 1:-1] +
			bilde[2:, 1:-1] +
			bilde[1:-1, 0:-2] +
			bilde[1:-1, 2:] -
			4 * bilde[1:-1, 1:-1])
	#bilde[1:-1, 1:-1] += alpha * laplace
	#im[:, 0] = im[:, 1]      # Neumann boundary
	#im[:, -1] = im[:, -2]    #
	#im[0, :] = im[1, :]      #
	#im[-1, :] = im[-2 , :]   #
	return laplace