#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
from funksjoner import laplacebilde
from time import sleep

def glatting(im):
    imc = np.copy(im)

    plt.ion()
    data = plt.imshow(imc, plt.cm.gray)
    plt.draw()

    # Solve diffusion equation

    alpha = .25                     # dt / dx**2
    for i in range(100):
        imc[1:-1, 1:-1] += alpha * laplacebilde(imc)
        imc[:, 0] = imc[:, 1]      # Neumann boundary
        imc[:, -1] = imc[:, -2]    #
        imc[0, :] = imc[1, :]      #
        imc[-1, :] = imc[-2 , :]   #
    
        data.set_array(imc)
        plt.draw()
        plt.pause(1e-4)
    plt.close()