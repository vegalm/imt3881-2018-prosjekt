#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
from funksjoner import laplacebilde

def inpainting(im):

  mask = np.zeros(np.shape(im))
  mask[120:150, 120:150]=1
  
  imc = im*(1-mask)
  inpainte = np.copy(imc)

  plt.ion()
  data = plt.imshow(imc, plt.cm.gray)
  plt.draw()

  alpha = .25                     # dt / dx**2
  for i in range(200):
 
    inpainte[1:-1, 1:-1] += alpha * laplacebilde(inpainte)
    imc = imc*(1-mask) + (inpainte*mask)

    data.set_array(imc)
    plt.draw()
    plt.pause(1e-4)
  plt.close()