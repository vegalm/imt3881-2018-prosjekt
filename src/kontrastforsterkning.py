#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
from funksjoner import laplacebilde

def kontrastf(im, imc1):

	k = 2                         #kontrastforsterking
	alpha = .20                     # dt / dx**2
	
	im0 = laplacebilde(im)
	imc1 = laplacebilde(imc1)

	im[1:-1, 1:-1] += (im0 - (k * imc1)) * alpha

	return im


def kontrastforsterkning(im):
	# Read image and add noise

	imc = np.copy(im)
	imc1 = np.copy(imc)

	plt.ion()
	data = plt.imshow(im, plt.cm.gray)
	plt.draw()

	for i in range(100):

		imc = kontrastf(imc, imc1)
		np.clip(imc, 0, 1, imc)

		data.set_array(imc)
		plt.draw()
		plt.pause(1e-4)
	plt.close()