#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import funksjoner as funk
from glatting import glatting
from inpainting import inpainting
from kontrastforsterkning import kontrastforsterkning 
from demosaicing import demosaicing 
from sømløskloning import sømløskloning

def main():

	im = plt.imread('lena.png')
	lena = plt.imread('lena2.png')
	ruben = plt.imread('ruben3.png')     #laster bilde
	
	funk.printmeny()
	valg = funk.lesint(1, 9)
	while(valg < 9):
		
		if valg == 1: glatting(im)
		elif valg == 2: inpainting(im)
		elif valg == 3: kontrastforsterkning(im)
		elif valg == 4: demosaicing(im)
		elif valg == 5: sømløskloning(lena, ruben)
		elif valg == 6: funk.printmeny()
		elif valg == 9: print('avslutter program')

		valg = funk.lesint(1, 9)
		
main()