#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
from funksjoner import laplacebilde


def sømløskloninin(lena, ruben):
	alpha = .25                     # dt / dx**2

	rublap = laplacebilde(ruben)
	lenalap = laplacebilde(lena)

	lena[1:-1, 1:-1] += lenalap*alpha - rublap*alpha

	return lena

def sømløskloning(im, ruben):

	rubenc = np.copy(ruben)
	lenac = np.copy(im)

	plt.ion()
	data = plt.imshow(im, plt.cm.gray)
	plt.draw()
	
	
	rubenxy = rubenc[216:681, 0:145, :]
	lenaxy = lenac[216:681, 0:145, :]

	for i in range(100):
		
		im[216:681, 0:145, :] = sømløskloninin(lenaxy, rubenxy)

		np.clip(im, 0, 1, im)

		data.set_array(im)
		plt.draw()
		plt.pause(1e-4)
	plt.close()